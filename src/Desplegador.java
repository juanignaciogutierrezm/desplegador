import java.io.File;

import javax.servlet.ServletException;

import org.apache.catalina.Context;
import org.apache.catalina.Service;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.core.AprLifecycleListener;
import org.apache.catalina.core.StandardServer;
import org.apache.catalina.startup.ContextConfig;
import org.apache.catalina.startup.Tomcat;


public class Desplegador {

	public static void main(String[] args)  {
		System.out.println("USO: java -jar desplegador.jar  -Djava.security.egd=file:/dev/./urandom </NombreContexto> <ubicacionContexto> <puerto> <basedir> <jksfile> <keystorePass> <keyAlias>");
		try{
		String contexto=args[0];
		String warfile=args[1];
		int port = Integer.valueOf(args[2]);
		String basedir=args[3];
		String jksfile=args[4];
		String keystorePass=args[5];
		String keyAlias=args[6];
		
		//String contexto = "/Sistema";	
		//String warfile= "c:\\Sistema";
		//int port= 365;
		
		System.out.println("Desplegando "+contexto+" ("+warfile+") en puerto "+port);
		
		
		
		Tomcat tomcat = new Tomcat();
		tomcat.setPort(port);
		tomcat.setBaseDir(basedir);
		
		// Add AprLifecycleListener
		StandardServer server = (StandardServer)tomcat.getServer();
		AprLifecycleListener listener = new AprLifecycleListener();
		server.addLifecycleListener(listener);
		
        Connector httpsConnector = new Connector();
        httpsConnector.setPort(port);
        httpsConnector.setSecure(true);
        httpsConnector.setScheme("https");        
        httpsConnector.setAttribute("keystoreType", "JKS");        
        httpsConnector.setAttribute("clientAuth", "false");
        httpsConnector.setAttribute("sslProtocol", "TLS");
        httpsConnector.setAttribute("SSLEnabled", true);
        
        httpsConnector.setAttribute("keyAlias", keyAlias);
        httpsConnector.setAttribute("keystorePass", keystorePass);
        httpsConnector.setAttribute("keystoreFile", jksfile);
		
    
        
        httpsConnector.setAttribute("protocol", "HTTP/1.1");
        httpsConnector.setAttribute("maxThreads", "200");
        httpsConnector.setAttribute("protocol", "org.apache.coyote.http11.Http11AprProtocol");
	    
        
        Service service = tomcat.getService();
        service.addConnector(httpsConnector);
        tomcat.setConnector(httpsConnector);
        
		
		
		Context addWebapp = tomcat.addWebapp(contexto,warfile);

		//tomcat.getHost().setAutoDeploy(true);
		//tomcat.getHost().setDeployOnStartup(true);
		
		//ContextConfig contextConfig = new ContextConfig();
		//addWebapp.addLifecycleListener(contextConfig);
        //if (new File("c:\\web.xml").exists()) {
        //    contextConfig.setDefaultWebXml("c:\\web.xml");
        //} else {
        //    contextConfig.setDefaultWebXml("org/apache/catalin/startup/NO_DEFAULT_XML");
        //}
        //tomcat.getHost().addChild(addWebapp);
		
		
		
		
		tomcat.start();
		tomcat.getServer().await();
		}catch(Exception e){
			System.out.println("Error:");
			e.printStackTrace();
		}

	}	


}
